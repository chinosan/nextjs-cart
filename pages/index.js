
import Layout from '../components/Layout'
import Products from '../components/Products'
import { getLatestItems } from '../services/ItemService'
import style from '../styles/Home.module.css'
import styleProduct from '../styles/Product.module.css'

export default function Home( {items} ) {
  return (
    <Layout title="Bienvenidos">
      <div className={style.banner} >
        <div className={style.bannerBackground} >
          <div className={style.bannerInfo} >
            <h2>Shop the summer 2022 collection</h2>
            <p>Level up your confort this season with our new Winter collection</p>
          </div>
        </div>
      </div>

      <h3>Latest Products</h3>
        <div className={styleProduct.items}>
          {items &&
            items.map((item) => (
              <Products key={item.id} item={item} showAs="item" />
            ))}
        </div>
    </Layout>
  )
}

export async function getStaticProps(){
  const res = await getLatestItems()
  return {
    props:{
      items: res,
    },
  }
}