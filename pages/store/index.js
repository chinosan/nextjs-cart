import Layout from "../../components/Layout";
import {getItems} from '../../services/ItemService';
import Products from "../../components/Products";
import styleItems from '../../styles/Product.module.css';

export default function Index( {items} ){
  return (
    <Layout title="Productos">
      <h1>Store</h1>
      <div className={styleItems.items} >
        {
          items && items.map( (item) =>  
            <Products  key={item.id} item={item} showAs="Default" />
            // console.log(item.image)
          )
        }
      </div>
      
    </Layout>
  );
}

export async function getStaticProps(){
  const res = await getItems();   

  return {
    props: {
      items: res
    }
  }
}