import Layout from "../../components/Layout";
import Products from "../../components/Products";
import { getItemData, getPathsFromIds } from "../../lib/utils";

export default function productPage( {productInfo} ){
  return <Layout>
    <Products item={productInfo} showAs="Page" />
  </Layout>
}

export async function getStaticPaths(){
  const paths = await getPathsFromIds();

  return {
    paths:paths,
    fallback: false
  }
}

export async function getStaticProps( {params} ){
  const id = params.id;
  const product = await getItemData(id);

  return {
    props: {
      productInfo: product
    }
  }
}