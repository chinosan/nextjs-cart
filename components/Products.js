import Link from "next/link";
import Image from 'next/image';
import style from '../styles/Product.module.css';
import { convertToPath } from "../lib/utils";
import CartButton from "./CartButton";

export default function Products({item, showAs, qty}) {
  if(showAs==='Page'){
    return <div>
      <div className={style.page}>
        <div className={style.image} > 
          <Image
              src={item.data.image}
              alt={item.data.description}
              width={600}
              height={600}
          />
        </div>

        <div className={style.info} >
          <div>
            <h2>{item.data.title}</h2>
          </div>
          <div className={style.price} >${item.data.price}</div>
          <div>{item.data.description}</div>
          <div>
            <CartButton item={item.data}/>
          </div>
        </div>
      </div>
    </div>
  }
  if(showAs==="ListItem"){
    return <div className={style.listItem} >
      <div>
        <Image
          src={item.image}
          alt={item.description}
          width={100}
          height={100}
        >
        </Image>
      </div>
      <div>
        <div><h3>{item.title}</h3></div>
        <div>{item.price}</div>
        {qty===0 ? '': <div>{qty} units</div>}
        {qty===0 ? '': <div>Subtotal: ${qty * item.price}</div>}
        
        
      </div>
    </div>
  }
  return (
    <div className={style.item} >
      <div>
        <Link href={`/store/${convertToPath(item.title)}`}>
          <a>
            <Image
              src={item.image}
              alt={item.description}
              width={500}
              height={500}
            >
            </Image>
          </a>
        </Link>
      </div>

      <div>
        <h3>
          <Link href={'/store/ruta'}> 
            <a> {item.title} </a>
          </Link>
        </h3>
      </div>

      <div> ${item.price} </div>
      <div>
        <CartButton item={item}/>
      </div>
    </div>
  )
}